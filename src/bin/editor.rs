#[macro_use] extern crate log;
use clap::{ Arg, App };
use glob::glob;
use serde::Deserialize;
use simplelog::{ TermLogger, WriteLogger, LevelFilter, CombinedLogger,
                 TerminalMode, ColorChoice };
use std::{ error::Error, ffi::OsStr, fs::{ File, remove_file, copy },
           io::{ Write, prelude::* }, path::PathBuf, process::Command, };

#[derive(Deserialize,Debug)]
struct Config {
    h1_dir: String,
    globals: String,
    saugopti2: String,
    mp42plg_exe: String,
    saugopti2_exe: String,
    mprmerge_exe: String,
    mprxmerge_exe: String,
    mprxpreprocessor_exe: String,
    logfile: String,
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {

    let matches = App::new("MPREditor")
        .version("0.1.0")
        .author("Jeffrey Stoffers <jstoffers@uberpurple.com>")
        .about("Parser and commandline editor for MPR files")
        .arg(Arg::with_name("config")
             .short("c")
             .long("config")
             .help("The name of the configuration file to use.")
             .value_name("config"))
        .arg(Arg::with_name("mpr")
             .short("m")
             .long("mpr")
             .help("Convert the supplied mprx files to mpr."))
        .arg(Arg::with_name("mprx")
             .short("x")
             .long("mprx")
             .help("Convert the supplied mpr files to mprx."))
        .arg(Arg::with_name("files")
             .required(true)
             .min_values(1))
        .get_matches();

    // Default path is User/AppData/Roaming/MPREditor/config.toml
    // unless there is a config.toml file in the current directory
    // use value of -config or the default config files
    let mut default_path = dirs::config_dir()
        .expect("Unable to find default path config_dir!");
    default_path.push("MPREditor");
    default_path.push("config.toml");

    let local_config = PathBuf::from("./config.toml");
    if local_config.as_path().exists() {
        default_path = local_config;
    }

    let configuration_path = matches
        .value_of("config")
        .unwrap_or_else(|| default_path.to_str().unwrap());

    let mut f = match File::open(configuration_path) {
        Ok(f) => f,
        Err(e) => panic!("Unable to open configuration file! {}", e),
    };

    let mut configuration_data = Vec::<u8>::new();

    match f.read_to_end(&mut configuration_data) {
        Ok(v) => v,
        Err(e) => panic!("Unable to read configuration file! {}", e),
    };

    let config: Config = match toml::from_slice(&configuration_data) {
        Ok(config) => config,
        Err(e) => panic!("Unable to parse configuration file! {}", e),
    };

    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Info, simplelog::Config::default(),
                            TerminalMode::Mixed, ColorChoice::Auto),
            WriteLogger::new(LevelFilter::Info, simplelog::Config::default(),
                             File::create(&config.logfile)
                             .expect("Unable to create log file!")),
        ]
    ).expect("Unable to start logging!");

    info!("Logging Started");
    info!("Input: {:?}",matches);

    let mut files: Vec<String> = Vec::new();

    // Expand all globs and replace files with them if --glob
    for g in matches.values_of("files").unwrap() {
        let glb: Vec<_> = match glob(g) {
            Ok(g) => g,
            Err(e) => {
                error!("Error globbing: {}",e);
                panic!("Error globbing: {}",e);
            },
        }.collect();

        for i in glb {
            let glb = match i {
                Ok(glb) => glb,
                Err(e) => {
                    error!("No globs found! {}",e);
                    panic!("No globs found! {}",e);
                },
            };

            let glb_str = match glb.as_path().to_str() {
                Some(glb) => glb,
                None => {
                    error!("Unable to convert glob to path str");
                    panic!("Unable to convert glob to path str");
                },
            };

            files.push(glb_str.to_string());
        }
    }

    info!("Files: {:?}", files);

    if matches.is_present("mpr") {
        match convert_mpr(&config,files) {
            Ok(ok) => ok,
            Err(e) => {
                error!("Convert MPR Error: {}", e);
                panic!("Convert MPR Error: {}", e);
            }
        }
    } else if matches.is_present("mprx") {
        match convert_mprx(&config,files) {
            Ok(ok) => ok,
            Err(e) => {
                error!("Convert MPRX Error: {}", e);
                panic!("Convert MPRX Error: {}", e);
            }
        }
    } else {
        match add_pods(&config,files) {
            Ok(ok) => ok,
            Err(e) => {
                error!("Add Pods Error: {}", e);
                panic!("Add Pods Error: {}", e);
            }
        }
    }

    Ok(())
}

fn convert_mpr(config: &Config, files: Vec<String>)
               -> Result<(), Box<dyn Error + Send + Sync>> {

    for f in files {
        let file = PathBuf::from(&f);
        let filename = match file.as_path().file_stem().and_then(OsStr::to_str) {
            Some(ok) => ok,
            None => {
                error!("Unable to extract file name from file: {}", f);
                panic!("Unable to extract file name from file: {}", f);
            }
        }.to_string();

        let mut outfile = PathBuf::from(f);
        outfile.pop();
        outfile.push(format!("{}.mpr",filename));

        let cmd =Command::new(&config.mprxpreprocessor_exe)
            .arg("-exportMPR")
            .arg(file.as_path().to_str().unwrap())
            .arg(outfile.as_path().to_str().unwrap())
            .output()?;

        info!("{:#?}",cmd.status);
        info!("{}",String::from_utf8_lossy(&cmd.stdout));
        info!("{}",String::from_utf8_lossy(&cmd.stderr));
    }

    Ok(())
}

fn convert_mprx(config: &Config, files: Vec<String>)
               -> Result<(), Box<dyn Error + Send + Sync>> {

    for f in files {
        let file = PathBuf::from(&f);
        let filename = match file.as_path().file_stem().and_then(OsStr::to_str) {
            Some(ok) => ok,
            None => {
                error!("Unable to extract file name from file: {}", f);
                panic!("Unable to extract file name from file: {}", f);
            }
        }.to_string();

        let mut outfile = PathBuf::from(f);
        outfile.pop();
        outfile.push(format!("{}.mprx",filename));

        let cmd = Command::new(&config.mprxpreprocessor_exe)
            .arg(file.as_path().to_str().unwrap())
            .arg(outfile.as_path().to_str().unwrap())
            .output()?;

        info!("{:#?}",cmd.status);
        info!("{}",String::from_utf8_lossy(&cmd.stdout));
        info!("{}",String::from_utf8_lossy(&cmd.stderr));
    }

    Ok(())
}

fn add_pods(config: &Config, files: Vec<String>)
            -> Result<(), Box<dyn Error + Send + Sync>> {
    let mp42plg = PathBuf::from(&config.mp42plg_exe);
    let saugopti2_exe = PathBuf::from(&config.saugopti2_exe);
    let saugopti2 = PathBuf::from(&config.saugopti2);
    let mprxmerge_exe = PathBuf::from(&config.mprxmerge_exe);
    let mprmerge_exe = PathBuf::from(&config.mprmerge_exe);
    let h1_dir = PathBuf::from(&config.h1_dir);
    let mut h1 = h1_dir.clone();
    h1.push("h1");

    for a in files {
        let mut dir = PathBuf::from(&a);
        dir.pop();

        let file = PathBuf::from(&a);

        let extension = match file.as_path().extension().and_then(OsStr::to_str) {
            Some(ok) => ok,
            None => {
                error!("Unable to extract extension from file: {}",
                       file.as_path().to_str().unwrap());
                panic!("Unable to extract extension from file: {}",
                       file.as_path().to_str().unwrap());
            }
        }.to_string();

        let mut temp = PathBuf::from(&a);
        temp.pop();
        temp.push(format!("$temst$.{}",extension));

        let mut bat = dir.clone();
        bat.push("$bat$.bat");

        let mut mrg = dir.clone();
        mrg.push("$mrg$.mrg");

        let mut mrgl = dir.clone();
        mrgl.push("$mrgl$.mrgl");

        let mut plgout = dir.clone();
        plgout.push("$temst$.mpr");

        let mut plg = dir.clone();
        plg.push("$temst$.plg");

        let mut merged = dir.clone();
        merged.push(format!("{}.{}","$merged$",extension));

        match copy(file.as_path(),temp.as_path()) {
            Ok(_) => {},
            Err(e) => {
                error!("Error copying file: {}",e);
                panic!("Error copying file: {}",e);
            },
        };

        let mut batf = match File::create(bat.as_path().to_str().unwrap()) {
            Ok(ok) => ok,
            Err(e) => {
                error!("Error creating batch file: {}",e);
                panic!("Error creating batch file: {}",e);
            }
        };

        let mut mrgf = match File::create(mrg.as_path().to_str().unwrap()) {
            Ok(ok) => ok,
            Err(e) => {
                error!("Error creating mrg file: {}",e);
                panic!("Error creating mrg file: {}",e);
            }
        };

        let mut mrglf = match File::create(mrgl.as_path().to_str().unwrap()) {
            Ok(ok) => ok,
            Err(e) => {
                error!("Error creating mrgl file: {}",e);
                panic!("Error creating mrgl file: {}",e);
            }
        };


        write!(mrglf,"[Merge1]\nFileName={}",mrg.as_path().to_str().unwrap())?;

        write!(mrgf,"[Options]\nFileReplace=1\nVariableIndex=1\n\
                     ContourInsert=1\nNewCoordWithOffset=1\n\n[Destination]\n\
                     FileName={}\n\n[Master]\n\n[FILE1]\nFileName={}\n\n\
                     [FILE2]\nFileName={}\n",
               merged.as_path().to_str().unwrap(),
               file.as_path().to_str().unwrap(),
               plgout.as_path().to_str().unwrap())?;

        // mp42plg.exe -f=.mprx -h1=h1 -o=.plg -r -e=.err -SPOS2 -PLY=.ply
        writeln!(batf, "{}", format!("\"{}\" -f=\"{}\" -h1=\"{}\" -o=\"{}\" -r -SPOS2"
                                     ,mp42plg.as_path().to_str().unwrap()
                                     ,temp.as_path().to_str().unwrap()
                                     ,h1.as_path().to_str().unwrap()
                                     ,plg.as_path().to_str().unwrap()))?;

        // saugopti2.exe -c=saugopti2.ini -f=.plg -o=dir -t=60
        writeln!(batf, "{}", format!("\"{}\" -c=\"{}\" -f=\"{}\" -o=\"{}\" -t=60"
                                     ,saugopti2_exe.as_path().to_str().unwrap()
                                     ,saugopti2.as_path().to_str().unwrap()
                                     ,plg.as_path().to_str().unwrap()
                                     ,dir.as_path().to_str().unwrap()))?;

        writeln!(batf, "CD /D \"{}\"", h1_dir.as_path().to_str().unwrap())?;

        match extension.as_str() {
            // mprmerge.exe -a=.mpr(x) -k -s=.mpr [ .mpr 0,0,0 0,0,0 0,0,0 ]
            "mpr" => writeln!(batf, "{}",
                              format!("\"{}\" -a=\"{}\" -k -s=\"{}\" [ \"{}\" 0,0,0 0,0,0 0,0,0 ]"
                                      ,mprmerge_exe.as_path().to_str().unwrap()
                                      ,merged.as_path().to_str().unwrap()
                                      ,plgout.as_path().to_str().unwrap()
                                      ,file.as_path().to_str().unwrap()))?,
            "mprx" => writeln!(batf, "{}",
                               format!("\"{}\" -fl=\"{}\""
                                       ,mprxmerge_exe.as_path().to_str().unwrap()
                                       ,mrgl.as_path().to_str().unwrap()))?,
            _ => {
                error!("Unknown extension!");
                panic!("Unknown extension!");
            },
        }

        let cmd = Command::new("cmd").arg("/C").arg(bat.as_path().to_str()
                                                    .unwrap()).output()?;
        info!("{:#?}",cmd.status);
        info!("{}",String::from_utf8_lossy(&cmd.stdout));
        info!("{}",String::from_utf8_lossy(&cmd.stderr));

        if merged.as_path().exists() {
            let mut old = file.clone();
            let name = file.as_path().file_name().and_then(OsStr::to_str)
                .unwrap().to_string();

            old.pop();
            old.push(format!("{}.bak",name));

            copy(file.as_path(),old.as_path())?;
            copy(merged.as_path(),file.as_path())?;

            if bat.as_path().exists() {
                remove_file(bat.as_path())?;
            }

            if merged.as_path().exists() {
                remove_file(merged.as_path())?;
            }
            if mrg.as_path().exists() {
                remove_file(mrg.as_path())?;
            }
            if mrgl.as_path().exists() {
                remove_file(mrgl.as_path())?;
            }
            if temp.as_path().exists() {
                remove_file(temp.as_path())?;
            }
            if plgout.as_path().exists() {
                remove_file(plgout.as_path())?;
            }
            if plg.as_path().exists() {
                remove_file(plg.as_path())?;
            }

        } else {
            panic!("Merged file was not created.");
        }

    }

    Ok(())
}
