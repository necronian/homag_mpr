use clap::{Arg, App};
use homag_mpr::MPR;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;

fn read_file_to_utf8(s: PathBuf) -> String {
    // Read file into Vec<u8>
    let mut file = File::open(s).expect("Unable to open the file");

    let mut contents: Vec<u8> = Vec::new();
    file.read_to_end(&mut contents).expect("Unable to read the file");

    // Convert iso-8859-1 to UTF8
    contents.iter().map(|&c| c as char).collect()
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let matches = App::new("MPRParse")
        .version("0.1.0")
        .author("Jeffrey Stoffers <jstoffers@uberpurple.com>")
        .about("Parser and commandline editor for MPR files")
        .arg(Arg::with_name("globals")
             .short("g")
             .long("globals")
             .help("Specify the location of the global variables.")
             .value_name("GLOBALS"))
        .arg(Arg::with_name("test")
             .short("t")
             .long("test")
             .help("Try parsing all MPR files in the supplied input directory recursivly."))
        .arg(Arg::with_name("file")
             .help("The name of the file/Directory to process.")
             .required(true)
             .index(1))
        .get_matches();

    let file = matches.value_of("file").unwrap();

    if matches.is_present("test") {

    } else {
        let file = PathBuf::from(file);
        let data = MPR::parse(&read_file_to_utf8(file));
        
        println!("{:?}",data);
    }
    Ok(())
}
