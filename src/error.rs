use std::fmt;
use crate::tokenizer::Level;
use crate::token::Token;

#[derive(Debug)]
pub enum Error {
    UnexpectedToken {
        typ: String,
        start: usize,
        end: usize,
        line: usize,
        column: usize,
    },
    TokenizeError {
        c: char,
        pos: usize,
        line: usize,
        column: usize,
        level: Level,
        next_literal: bool,
    },
    UnexpectedTokenEnd,
    UnmatchedTokenType(Token),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::UnexpectedToken {
                typ,
                start,
                end,
                line,
                column,
            } => write!(
                f,
                "An Unexpected Token was Encountered! Of {} @ ({},{}) from ({}:{})",
                typ, line, column, start, end
            ),
            Error::TokenizeError {
                c,
                pos,
                line,
                column,
                level,
                next_literal,
            } => write!(
                f,
                "An Error was Encountered Tokenizing the Input! Found {} @ {} ({},{}) - Level: {} - Next is Literal: {}",
                c, pos, line, column, level, next_literal
            ),
            Error::UnexpectedTokenEnd => write!(f,"Unexpected Token End"),
            Error::UnmatchedTokenType(t) => write!(f,"Unmatched Token Type: {}",t),
        }
    }
}
