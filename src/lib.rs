mod token;
mod tokenizer;
mod error;
mod parser;

use std::collections::HashMap;
use token::Token;
use tokenizer::Tokenizer;
use parser::Parser;

#[derive(Default, Debug, Clone)]
pub struct MPR {
    pub header: Vec<String>,
    pub variables: Vec<String>,
    pub coordinates: Vec<String>,
    pub contours: Vec<String>,
    pub macros: Vec<String>,
    pub embeds: Vec<String>,
    pub environment: HashMap<String, f64>,
}

impl MPR {
    pub fn parse(mpr: &str) -> Self {
        let tokenizer = Tokenizer::new(mpr);
        let environment: HashMap<String,f64> = HashMap::new();

        // for t in tokenizer {
        //     println!("{}",t);
        // }

        let mut parser = Parser::new(&environment,tokenizer);

        let parsed = parser.parse();

        println!("Lib Error: {:?}",parsed);

        MPR {
            header: Vec::new(),
            variables: Vec::new(),
            coordinates: Vec::new(),
            contours: Vec::new(),
            macros: Vec::new(),
            embeds: Vec::new(),
            environment: HashMap::new(),
        }
    }
}
