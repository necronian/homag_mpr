use std::collections::HashMap;

use crate::error::Error;
use crate::token::{Token, TokenType as TT};
use crate::tokenizer::Tokenizer;

#[derive(Debug)]
pub struct NodeMeta {
    pub origional: Token,
    pub metadata: Vec<Token>,
}

#[derive(Debug)]
pub enum Node {
    Empty,
    Head(NodeMeta, String, String),
    Number(NodeMeta, f64),
    EOF(NodeMeta),
}

#[derive(Debug)]
pub struct Parser<'a, 'b> {
    tokens: Tokenizer<'a>,
    env: &'b HashMap<String, f64>,
    meta: Vec<TT>,
}

impl<'a, 'b> Parser<'a, 'b> {
    pub fn new(env: &'b HashMap<String, f64>, tokens: Tokenizer<'a>) -> Parser<'a, 'b> {
        Parser {
            env,
            tokens,
            meta: Vec::new(),
        }
    }

    // Binding Power
    pub fn bp(&self, t: &Token) -> usize {
        match t.typ {
            TT::Head(_) => 1,
            TT::Whitespace(_) => 1,
            TT::HeadValue(_) => 998,
            TT::HeadKey(_) => 999,
            TT::HeadEquals(_) => 999,
            _ => panic!("Need a bp for the token type: {}", t.typ),
        }
    }

    // Prefix (Null Denotation)
    pub fn nud(&mut self, t: Token, _bp: usize) -> Result<Node, Error> {
        match t.typ {
            TT::HeadKey(ref n) => {
                let right = self.expr(self.bp(&t))?;
                Ok(Node::Head(n.to_string(), Box::new(right)))
            }
            // TT::HeadEquals(n) => Ok(Node::Literal("=".to_string())),
            // TT::Head(n) => Ok(Node::Section(n)),
            token @ TT::Whitespace(_) => {
                self.meta.push(token);
                Ok(Node::Empty)
            }
            token @ TT::Head(_) => {
                self.meta.push(token);
                Ok(Node::Empty)
            }
            _ => Err(Error::UnmatchedTokenType(t)),
        }
    }

    // Infix (Left Denotation)
    pub fn led(&mut self, left: Node, op: Box<Token>, bp: usize) -> Result<Node, Error> {
        match op.typ {
            // TT::HeadKey(ref n) => {
            //     println!("RIGHT");
            //     let right = self.expr(self.bp(&op))?;
            //     Ok(Node::Head(n.to_string(),Box::new(right)))
            // },
            //TT::HeadEquals(n) => Ok(Node::Literal("=".to_string())),
            //TT::Whitespace(n) => Ok(Node::Whitespace(n)),
            token @ TT::Whitespace(_) => {
                self.meta.push(token);
                Ok(Node::Empty)
            }
            _ => Err(op.error()),
        }
    }

    pub fn expr(&mut self, rbp: usize) -> Result<Node, Error> {
        let first_t = self.tokens.next().ok_or(Error::UnexpectedTokenEnd)?;
        let first_t_bp = self.bp(&first_t);

        let mut left = self.nud(*first_t, first_t_bp)?;
        if self.tokens.peek().is_none() {
            return Ok(left);
        }

        let mut peeked = self.tokens.peek_owned();
        while peeked.is_some() && rbp < self.bp(&peeked.unwrap()) {
            let op = self.tokens.next().ok_or(Error::UnexpectedTokenEnd)?;
            let op_bp = self.bp(&op);
            left = self.led(left, op, op_bp)?;

            if self.tokens.peek().is_none() {
                break;
            }

            peeked = self.tokens.peek_owned();
        }

        Ok(left)
    }

    pub fn parse(&mut self) -> Result<Node, Error> {
        let result = self.expr(0);
        println!("Self: {:?}", self);
        println!("After result: {:?}", result);
        match self.tokens.next() {
            Some(t) => Err(t.error()),
            None => result,
        }
    }
}
