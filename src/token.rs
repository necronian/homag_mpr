use std::fmt;
use crate::error::Error;

// Track the token type, value and other metadata
#[derive(Debug, Clone)]
pub struct Token {
    pub typ: TokenType,
    pub start: usize,
    pub end: usize,
    pub line: usize,
    pub column: usize,
}

impl Token {
    pub fn new( typ: TokenType, start: usize, end: usize,
                line: usize, column: usize, ) -> Token {
        Token {
            typ,
            start,
            end,
            line,
            column,
        }
    }

    pub fn error(&self) -> Error {
        Error::UnexpectedToken {
            typ: self.typ.to_string(),
            start: self.start,
            end: self.end,
            line: self.line,
            column: self.column,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    Whitespace(String),
    Ply(String),
    // Top Level Heading Attributes
    Head(String),
    HeadKey(String),
    HeadEquals(String),
    HeadValue(String),
    EndHeadValue(String),
    // Top Level Variable Attributes
    Variable(String),
    VariableKey(String),
    VariableEquals(String),
    VariableValue(String),
    EndVariableValue(String),
    // Top Level Coordinate Attributes
    Coordinate(String),
    CoordinateName(String),
    CoordinateKey(String),
    CoordinateEquals(String),
    CoordinateValue(String),
    EndCoordinateValue(String),
    // Top Level Contour Attributes
    ContourNumber(String),
    ContourElementNumber(String),
    ContourElement(String),
    ContourComment(String),
    ContourKey(String),
    ContourEquals(String),
    ContourValue(String),
    EndContourValue(String),
    // Top Level Macro Attributes
    MacroId(String),
    MacroName(String),
    MacroKey(String),
    MacroEquals(String),
    MacroValue(String),
    EndMacroValue(String),
    // Top Level Attributes
    Comment(String),
    EOF(String),
    Literal(String),
    // Value Level Tokens
    Abs(String),
    Add(String),
    And(String),
    ArcCos(String),
    ArcSin(String),
    ArcTan(String),
    CloseParen(String),
    Cos(String),
    Divide(String),
    Else(String),
    Equal(String),
    Exp(String),
    Exponent(String),
    GreaterThan(String),
    GreaterThanOrEqual(String),
    If(String),
    LessThan(String),
    LessThanOrEqual(String),
    Ln(String),
    Mod(String),
    Multiply(String),
    Not(String),
    NotEqual(String),
    Number(String),
    OpenParen(String),
    Or(String),
    Prec(String),
    Relative(String),
    Sin(String),
    Sqrt(String),
    Standard(String),
    Subtract(String),
    Tan(String),
    Then(String),
    Var(String),
    CounterClockwiseSmall(String),//Constant 0
    ClockwiseSmall(String),//Constant 1
    CounterClockwiseBig(String),//Constant 2
    ClockwiseBig(String),//Constant 3
    Ok(String),//Constant 1
    No(String),//Constant 0
    Mirror(String),
    NonMirror(String),
    YMirror(String),
    NonYMirror(String),
    BSX(String),
    BSY(String),
    BSZ(String),
}

/// Quoted or unquoted values
#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Value(String),
    QuotedValue(String),    
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} - L:{} C:{} Pos: ({}-{})",
               self.typ,
               self.line,
               self.column,
               self.start,
               self.end,
        )
    }
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            TokenType::Whitespace(v) => format!("Whitespace: {:?}",v),
            TokenType::Ply(v) => format!("Ply: {}",v),
            TokenType::Head(v) => format!("Head: {}",v),
            TokenType::HeadKey(v) => format!("Head Key: {}",v),
            TokenType::HeadEquals(v) => format!("Head Equals: {}",v),
            TokenType::HeadValue(v) => format!("Head Value: {}",v),
            TokenType::EndHeadValue(v) => format!("End Head Value: {}",v),
            TokenType::Variable(v) => format!("Variable: {}",v),
            TokenType::VariableKey(v) => format!("Variable Key: {}",v),
            TokenType::VariableEquals(v) => format!("Variable Equals: {}",v),
            TokenType::VariableValue(v) => format!("Variable Value: {}",v),
            TokenType::EndVariableValue(v) => format!("End Variable Value: {}",v),
            TokenType::Coordinate(v) => format!("Coordinate: {}",v),
            TokenType::CoordinateName(v) => format!("Coordinate Name: {}",v),
            TokenType::CoordinateKey(v) => format!("Coordinate Key: {}",v),
            TokenType::CoordinateEquals(v) => format!("Coordinate Equals: {}",v),
            TokenType::CoordinateValue(v) => format!("Coordinate Value: {}",v),
            TokenType::EndCoordinateValue(v) => format!("End Coordinate Value: {}",v),
            TokenType::ContourNumber(v) => format!("Contour Number: {}",v),
            TokenType::ContourElementNumber(v) => format!("Contour Element Number: {}",v),
            TokenType::ContourElement(v) => format!("Contour Element: {}",v),
            TokenType::ContourComment(v) => format!("Contour Comment: {}",v),
            TokenType::ContourKey(v) => format!("Contour Key: {}",v),
            TokenType::ContourEquals(v) => format!("Contour Equals: {}",v),
            TokenType::ContourValue(v) => format!("Contour Value: {}",v),
            TokenType::EndContourValue(v) => format!("End Contour Value: {}",v),
            TokenType::MacroId(v) => format!("Macro Id: {}",v),
            TokenType::MacroName(v) => format!("Macro Name: {}",v),
            TokenType::MacroKey(v) => format!("Macro Key: {}",v),
            TokenType::MacroEquals(v) => format!("Macro Equals: {}",v),
            TokenType::MacroValue(v) => format!("Macro Value: {}",v),
            TokenType::EndMacroValue(v) => format!("End Macro Value: {}",v),
            TokenType::Comment(v) => format!("Comment: {}",v),
            TokenType::EOF(v) => format!("EOF: {}",v),
            TokenType::Literal(v) => format!("Literal: {}",v),
            TokenType::Abs(v) => format!("Abs: {}",v),
            TokenType::Add(v) => format!("Add: {}",v),
            TokenType::And(v) => format!("And: {}",v),
            TokenType::ArcCos(v) => format!("Arc Cos: {}",v),
            TokenType::ArcSin(v) => format!("Arc Sin: {}",v),
            TokenType::ArcTan(v) => format!("Arc Tan: {}",v),
            TokenType::CloseParen(v) => format!("Close Paren: {}",v),
            TokenType::Cos(v) => format!("Cos: {}",v),
            TokenType::Divide(v) => format!("Divide: {}",v),
            TokenType::Else(v) => format!("Else: {}",v),
            TokenType::Equal(v) => format!("Equal: {}",v),
            TokenType::Exp(v) => format!("Exp: {}",v),
            TokenType::Exponent(v) => format!("Exponent: {}",v),
            TokenType::GreaterThan(v) => format!("Greater Than: {}",v),
            TokenType::GreaterThanOrEqual(v) => format!("Greater Than or Equal: {}",v),
            TokenType::If(v) => format!("If: {}",v),
            TokenType::LessThan(v) => format!("Less Than: {}",v),
            TokenType::LessThanOrEqual(v) => format!("Less Than or Equal: {}",v),
            TokenType::Ln(v) => format!("Ln: {}",v),
            TokenType::Mod(v) => format!("Mod: {}",v),
            TokenType::Multiply(v) => format!("Multiply: {}",v),
            TokenType::Not(v) => format!("Not: {}",v),
            TokenType::NotEqual(v) => format!("NotEqual: {}",v),
            TokenType::Number(v) => format!("Number: {}",v),
            TokenType::OpenParen(v) => format!("OpenParen: {}",v),
            TokenType::Or(v) => format!("Or: {}",v),
            TokenType::Prec(v) => format!("Prec: {}",v),
            TokenType::Relative(v) => format!("Relative: {}",v),
            TokenType::Sin(v) => format!("Sin: {}",v),
            TokenType::Sqrt(v) => format!("Sqrt: {}",v),
            TokenType::Standard(v) => format!("Standard: {}",v),
            TokenType::Subtract(v) => format!("Subtract: {}",v),
            TokenType::Tan(v) => format!("Tan: {}",v),
            TokenType::Then(v) => format!("Then: {}",v),
            TokenType::Var(v) => format!("Var: {}",v),
            TokenType::CounterClockwiseSmall(v) => format!("Counter Clockwise Small: {}",v),
            TokenType::ClockwiseSmall(v) => format!("Clockwise Small: {}",v),
            TokenType::CounterClockwiseBig(v) => format!("Counter Clockwise Big: {}",v),
            TokenType::ClockwiseBig(v) => format!("Clockwise Big: {}",v),
            TokenType::Ok(v) => format!("Ok: {}",v),
            TokenType::No(v) => format!("No: {}",v),
            TokenType::Mirror(v) => format!("Mirror: {}",v),
            TokenType::NonMirror(v) => format!("Non Mirror: {}",v),
            TokenType::YMirror(v) => format!("Y-Mirror: {}",v),
            TokenType::NonYMirror(v) => format!("Non Y-Mirror: {}",v),
            TokenType::BSX(v) => format!("BSX: {}",v),
            TokenType::BSY(v) => format!("BSY: {}",v),
            TokenType::BSZ(v) => format!("BSZ: {}",v),
        })
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Value::Value(d) => d,
            Value::QuotedValue(d) => d,
        })
    }
}
