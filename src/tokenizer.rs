use crate::error::Error;
use crate::token::{Token, TokenType as TT, Value};
use std::fmt;

// Keep track of where we are in the parse tree. Right now just used to check
// if we have parsed a descriptor and are on the other side of the
// descriptor's equals sign
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Level {
    Top,
    Head,
    Variable,
    Coordinate,
    Contour,
    Macro,

    HeadKey,
    HeadEquals,
    HeadValue,

    VariableKey,
    VariableEquals,
    VariableValue,

    CoordinateName,
    CoordinateKey,
    CoordinateEquals,
    CoordinateValue,

    ContourNumber,
    ContourElementNumber,
    ContourElement,
    ContourComment,
    ContourKey,
    ContourEquals,
    ContourValue,

    MacroName,
    MacroKey,
    MacroEquals,
    MacroValue,

    EmbedPly,
}

impl fmt::Display for Level {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}",
               match self {
                   Level::Top => "Top",
                   Level::Head => "Head",
                   Level::Variable => "Variable",
                   Level::Coordinate => "Coordinate",
                   Level::Contour => "Contour",
                   Level::Macro => "Macro",
                   Level::HeadKey => "HeadKey",
                   Level::HeadEquals => "HeadEquals",
                   Level::HeadValue => "HeadValue",
                   Level::VariableKey => "VariableKey",
                   Level::VariableEquals => "VariableEquals",
                   Level::VariableValue => "VariableValue",
                   Level::CoordinateName => "CoordinateName",
                   Level::CoordinateKey => "CoordinateKey",
                   Level::CoordinateEquals => "CoordinateEquals",
                   Level::CoordinateValue => "CoordinateValue",
                   Level::ContourNumber => "ContourNumber",
                   Level::ContourElementNumber => "ContourElementNumber",
                   Level::ContourElement => "ContourElement",
                   Level::ContourComment => "ContourComment",
                   Level::ContourKey => "ContourKey",
                   Level::ContourEquals => "ContourEquals",
                   Level::ContourValue => "ContourValue",
                   Level::MacroName => "MacroName",
                   Level::MacroKey => "MacroKey",
                   Level::MacroEquals => "MacroEquals",
                   Level::MacroValue => "MacroValue",
                   Level::EmbedPly => "EmbedPly",
               }
        )
    }
}

// Struct to handle tokenizing a passed enumerable char string
#[derive(Debug)]
pub struct Tokenizer<'a> {
    position: usize,
    peeked: Option<Box<Token>>, // TODO: Why did I box this?
    chars: std::iter::Peekable<std::iter::Enumerate<std::str::Chars<'a>>>,
    current_line: usize,
    current_column: usize,
    level: Level,
    next_literal: bool,
}

impl<'a> Tokenizer<'a> {
    pub fn error(&self, c: char) -> Error {
        Error::TokenizeError {
            c,
            pos: self.position,
            line: self.current_line,
            column: self.current_column,
            level: self.level,
            next_literal: self.next_literal,
        }
    }

    pub fn new(source: &'a str) -> Tokenizer {
        Tokenizer {
            position: 0,
            peeked: None,
            chars: source.chars().enumerate().peekable(),
            current_line: 1,
            current_column: 1,
            level: Level::Top,
            next_literal: false,
        }
    }

    fn eat_digits (&mut self) -> String {
        let mut digit = self.next_char()
            .expect("Cannot take expected digit from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if !c.is_digit(10) {
                break;
            }
            digit = format!("{}{}", digit, self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        digit
    }

    fn eat_number (&mut self) -> String {
        let mut digit = self.next_char()
            .expect("Cannot take expected number from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if !c.is_digit(10) && c != '.' && c != 'e' && c != 'E' {
                break;
            }
            digit = format!("{}{}", digit, self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        digit
    }

    fn eat_whitespace (&mut self) -> String {
        let mut whitespace = self.next_char()
            .expect("Cannot take expected whitespace from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if !c.is_whitespace() {
                break;
            }
            whitespace = format!("{}{}", whitespace, self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        whitespace
    }

    fn eat_until_newline (&mut self) -> String {
        let mut data = self.next_char()
            .expect("Cannot take expected data from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if c == '\n' || c == '\r' {
                break;
            }
            data = format!("{}{}", data, self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        data
    }

    fn eat_identifier (&mut self) -> String {
        let mut data = self.next_char()
            .expect("Cannot take expected data from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if !('A'..='Z').contains(&c) && !('a'..='z').contains(&c) &&
                !('0'..='9').contains(&c) && c != '_' && c != '.' && c != '?'
            {
                break;
            }
            data = format!("{}{}", data, self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        data
    }

    fn eat_key (&mut self) -> Value {
        let mut data = self.next_char()
            .expect("Cannot take expected data from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        if data == "\"" {
            data = "".to_string();
            while let Some(c) = peek {
                if c == '"' {
                    self.next_char();
                    break;
                }
                data = format!("{}{}", data, self.next_char().unwrap());
                peek = self.peek_char().cloned();
            }
            Value::QuotedValue(data)
        } else {
            while let Some(c) = peek {
                if c == '\r' || c == '\n' {
                    break;
                }
                data = format!("{}{}", data, self.next_char().unwrap());
                peek = self.peek_char().cloned();
            }
            Value::Value(data)
        }
    }

    // Return a String of all consecutive whitespace characters or None
    fn whitespace(&mut self, start: usize, line: usize, column: usize)
                  -> Option<Token> {
        let ws = self.eat_whitespace();

        match (self.level,ws.contains('\n')) {
            // If the Contour Element hits a newline before there are more
            // characters then the Element has no Comment and we need to reset
            // the level
            (Level::ContourElement, true) => {
                self.level = Level::Contour;
            },
            (Level::MacroName, true) => {
                self.level = Level::Macro;
            }
            (_,_) => {}
        }

        Some(Token::new(TT::Whitespace(ws),
                        start,
                        self.position,
                        line,
                        column))
    }

    fn macro_name(&mut self, start: usize, line: usize, column: usize)
                  -> Option<Token> {
        let mut mac = self.next_char()
            .expect("Cannot take expected macroname from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if c == '\\' {
                self.next_char();
                break;
            }
            mac = format!("{}{}", mac, self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        self.level = Level::Macro;
        Some(Token::new(TT::MacroName(mac),
                        start,
                        self.position,
                        line,
                        column))
    }

    fn coordinate_name(&mut self, start: usize, line: usize, column: usize)
                       -> Option<Token> {
        let mut coordinate = self.next_char()
            .expect("Cannot take expected coordinatename from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if c == '\\' {
                self.next_char();
                break;
            }
            coordinate = format!("{}{}", coordinate, self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        if coordinate == "<00" {
            self.level = Level::Coordinate;
            Some(Token::new(TT::CoordinateName(coordinate),
                            start,
                            self.position,
                            line,
                            column))
        } else {
            self.level = Level::Macro;
            Some(Token::new(TT::MacroName(coordinate),
                            start,
                            self.position,
                            line,
                            column))
        }

    }

    fn comment(&mut self, start: usize, line: usize, column: usize)
               -> Option<Token> {
        let comment = self.eat_until_newline();
        Some(Token::new(TT::Comment(comment),
                        start,
                        self.position,
                        line,
                        column))
    }

    fn contour_element_number(&mut self, start: usize, line: usize,
                              column: usize) -> Option<Token> {
        let mut contour_element = self.next_char()
            .expect("Cannot take expected contour_elementnumber from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if c == '\\' || c == '\n' || c == '\r' {
                break;
            }
            contour_element = format!("{}{}", contour_element,
                                      self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        self.level = Level::ContourElementNumber;
        Some(Token::new(TT::ContourElementNumber(contour_element),
                        start,
                        self.position,
                        line,
                        column))
    }

    fn contour_element(&mut self, start: usize, line: usize, column: usize)
                       -> Option<Token> {
        let mut contour_element = self.next_char()
            .expect("Cannot take expected contour_elementnumber from stream.")
            .to_string();
        let mut peek = self.peek_char().cloned();

        while let Some(c) = peek {
            if c == ' ' || c == '\r' || c == '\n' {
                break;
            }
            contour_element = format!("{}{}", contour_element,
                                      self.next_char().unwrap());
            peek = self.peek_char().cloned();
        }

        self.level = Level::ContourElement;
        Some(Token::new(TT::ContourElement(contour_element),
                        start,
                        self.position,
                        line,
                        column))
    }

    // Return the next character in the token stream without consuming it
    fn peek_char(&mut self) -> Option<&char> {
        match self.chars.peek() {
            Some((_, c)) => Some(c),
            None => None,
        }
    }

    // Take the next character from the Token stream
    fn next_char(&mut self) -> Option<char> {
        match self.chars.next() {
            Some((_, e)) => {
                // Increment global position
                self.position += 1;
                // Increment Column Position, or Reset Column and increment
                // Line on newline
                match e {
                    '\n' => {
                        self.current_line += 1;
                        self.current_column = 1;
                    }
                    _ => {
                        self.current_column += 1;
                    }
                }
                // Return the Character
                Some(e)
            }
            None => None,
        }
    }

    // Returns the next token in the stream without advancing the iterator
    pub fn peek(&mut self) -> &Option<Box<Token>> {
        // If we already calculated a peeked token return it
        if self.peeked.is_some() {
            return &self.peeked;
        }

        let start = self.position;
        let line = self.current_line;
        let column = self.current_column;
        let peeked_char = self.peek_char().cloned(); // TODO: Why .cloned()?

        let token = match peeked_char {
            Some(c) => match c {
                'S' if self.level == Level::Macro => {
                    let mut identifier = self.eat_identifier();
                    let mut peek = self.peek_char().cloned();

                    if identifier == "STARTLOCAL" {
                        while let Some(c) = peek {
                            let data = if c.is_whitespace() {
                                self.eat_whitespace()
                            } else {
                                self.eat_until_newline()
                            };
                            if data == "ENDLOCAL" {
                                identifier = format!("{}{}", identifier, data);
                                break;
                            }
                            identifier = format!("{}{}", identifier, data);
                            peek = self.peek_char().cloned();
                        }
                        
                        Some(Token::new(
                            TT::Ply(identifier),
                            start,
                            self.position,
                            line,
                            column))
                    } else {
                        self.level = Level::MacroKey;
                        Some(Token::new(
                            TT::MacroKey(identifier),
                            start,
                            self.position,
                            line,
                            column))
                    }
                },
                // MPR Macro Header or Coordinate System
                '<' => {
                    match self.level {
                        Level::Top | Level::Head | Level::Variable |
                        Level::Contour | Level::Macro  => {
                            self.level = Level::MacroName;
                            Some(Token::new(
                                TT::MacroName(self.eat_digits()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::Coordinate => {
                            self.level = Level::CoordinateName;
                            Some(Token::new(
                                TT::CoordinateName(self.eat_digits()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::HeadValue |
                        Level::VariableValue |
                        Level::CoordinateValue |
                        Level::ContourValue |
                        Level::MacroValue => {
                            let tkn = self.next_char().unwrap().to_string();
                            match self.peek_char() {
                                Some('=') => Some(Token::new(
                                    TT::LessThanOrEqual(
                                        format!("{}{}", tkn,
                                                self.next_char().unwrap().to_string())),
                                    start,
                                    self.position - 1,
                                    line,
                                    column)),
                                Some('>') => Some(Token::new(
                                    TT::NotEqual(
                                        format!("{}{}", tkn,
                                                self.next_char().unwrap().to_string())),
                                    start,
                                    self.position - 1,
                                    line,
                                    column)),
                                _ => Some(Token::new(
                                    TT::LessThan(tkn),
                                    start,
                                    self.position - 1,
                                    line,
                                    column)),
                            }
                        },
                        _ => panic!("Unexpected level <: {}",self.error('<')),
                    }

                },
                _ if self.next_literal
                    && (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => {
                        self.next_literal = false;
                        let mut data = self.next_char()
                            .expect("Cannot take expected data from stream.")
                            .to_string();
                        let mut peek = self.peek_char().cloned();
                        while let Some(c) = peek {
                            if c == '\n' || c == '\r' {
                                break;
                            }
                            data = format!("{}{}", data, self.next_char().unwrap());
                            peek = self.peek_char().cloned();
                        }

                        if data.ends_with('"') {
                            let (d, _) = data.split_at(data.len()-1);
                            data = d.to_string();
                        }

                        Some(Token::new(
                            TT::Literal(data),
                            start,
                            self.position,
                            line,
                            column))
                    },
                n if (n.is_digit(10) || n == '.') &&
                    (self.level == Level::HeadValue ||
                     self.level == Level::VariableValue ||
                     self.level == Level::CoordinateValue ||
                     self.level == Level::ContourValue ||
                     self.level == Level::MacroValue) =>
                    Some(Token::new(
                        TT::Number(self.eat_number()),
                        start,
                        self.position,
                        line,
                        column)),
                'A'..='Z' | 'a'..='z' | '0'..='9' | '_' if
                    (self.level == Level::HeadValue ||
                     self.level == Level::VariableValue ||
                     self.level == Level::CoordinateValue ||
                     self.level == Level::ContourValue ||
                     self.level == Level::MacroValue) => {
                        match self.eat_identifier().as_ref() {
                            "_cc" => Some(Token::new(
                                TT::CounterClockwiseSmall("_cc".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_cw" => Some(Token::new(
                                TT::ClockwiseSmall("_cw".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_CC" => Some(Token::new(
                                TT::CounterClockwiseBig("_CC".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_CW" => Some(Token::new(
                                TT::ClockwiseBig("_CW".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_mirror" => Some(Token::new(
                                TT::Mirror("_mirror".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_nonmirror" => Some(Token::new(
                                TT::NonMirror("_nonmirror".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_ymirror" => Some(Token::new(
                                TT::YMirror("_ymirror".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_nonymirror" => Some(Token::new(
                                TT::NonYMirror("_nonymirror".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_ok" => Some(Token::new(
                                TT::Ok("_ok".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "_no" => Some(Token::new(
                                TT::No("_no".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "ABS" => Some(Token::new(
                                TT::Abs("ABS".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "SIN" => Some(Token::new(
                                TT::Sin("SIN".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "COS" => Some(Token::new(
                                TT::Cos("COS".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "TAN" => Some(Token::new(
                                TT::Tan("TAN".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "ARCSIN" => Some(Token::new(
                                TT::ArcSin("ARCSIN".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "ARCCOS" => Some(Token::new(
                                TT::ArcCos("ARCCOS".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "ARCTAN" => Some(Token::new(
                                TT::ArcTan("ARCTAN".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "EXP" => Some(Token::new(
                                TT::Exp("EXP".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "LN" => Some(Token::new(
                                TT::Ln("LN".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "SQRT" => Some(Token::new(
                                TT::Sqrt("SQRT".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "MOD" => Some(Token::new(
                                TT::Mod("MOD".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "PREC" => Some(Token::new(
                                TT::Prec("PREC".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "AND" => Some(Token::new(
                                TT::And("AND".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "OR" => Some(Token::new(
                                TT::Or("OR".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "NOT" => Some(Token::new(
                                TT::Not("NOT".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "IF" => Some(Token::new(
                                TT::If("IF".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "THEN" => Some(Token::new(
                                TT::Then("THEN".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "ELSE" => Some(Token::new(
                                TT::Else("ELSE".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            "STANDARD" => Some(Token::new(
                                TT::Standard("STANDARD".to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                            v => Some(Token::new(
                                TT::Var(v.to_string()),
                                start,
                                self.position,
                                line,
                                column)),
                        }
                    },
                '@' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::Relative(self.next_char().unwrap().to_string()),
                            start,
                            self.position,
                            line,
                            column)),
                '=' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) =>
                    Some(Token::new(
                        TT::Equal(self.next_char().unwrap().to_string()),
                        start,
                        self.position - 1,
                        line,
                        column)),
                '>' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => {
                    let tkn = self.next_char().unwrap().to_string();
                    match self.peek_char() {
                        Some('=') => Some(Token::new(
                            TT::GreaterThanOrEqual(
                                format!("{}{}", tkn,
                                        self.next_char().unwrap().to_string())),
                            start,
                            self.position - 1,
                            line,
                            column)),
                        _ => Some(Token::new(
                            TT::GreaterThan(tkn),
                            start,
                            self.position - 1,
                            line,
                            column,
                        )),
                    }
                },
                '*' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::Multiply(self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column)),
                '/' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::Divide(self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column)),
                '+' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::Add(self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column)),
                '-' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::Subtract(self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column)),
                '^' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::Exponent(self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column)),
                '(' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::OpenParen(self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column)),
                ')' if (self.level == Level::HeadValue ||
                        self.level == Level::VariableValue ||
                        self.level == Level::CoordinateValue ||
                        self.level == Level::ContourValue ||
                        self.level == Level::MacroValue) => Some(Token::new(
                            TT::CloseParen(self.next_char().unwrap().to_string()),
                            start,
                            self.position - 1,
                            line,
                            column)),
                '"' if self.level == Level::HeadValue => {
                    self.level = Level::Head;
                    self.next_char();
                    Some(Token::new(
                        TT::EndHeadValue("\"".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '\n' | '\r' if self.level == Level::HeadValue => {
                    self.level = Level::Head;
                    Some(Token::new(
                        TT::EndHeadValue("".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '"' if self.level == Level::VariableValue => {
                    self.level = Level::Variable;
                    self.next_char();
                    Some(Token::new(
                        TT::EndVariableValue("\"".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '\n' | '\r' if self.level == Level::VariableValue => {
                    self.level = Level::Variable;
                    Some(Token::new(
                        TT::EndVariableValue("".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '"' if self.level == Level::CoordinateValue => {
                    self.level = Level::Coordinate;
                    self.next_char();
                    Some(Token::new(
                        TT::EndCoordinateValue("\"".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '\n' | '\r' if self.level == Level::CoordinateValue => {
                    self.level = Level::Coordinate;
                    Some(Token::new(
                        TT::EndCoordinateValue("".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '"' if self.level == Level::ContourValue => {
                    self.level = Level::Contour;
                    self.next_char();
                    Some(Token::new(
                        TT::EndContourValue("\"".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '\n' | '\r' if self.level == Level::ContourValue => {
                    self.level = Level::Contour;
                    Some(Token::new(
                        TT::EndContourValue("".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '"' if self.level == Level::MacroValue => {
                    self.level = Level::Macro;
                    self.next_char();
                    Some(Token::new(
                        TT::EndMacroValue("\"".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                '\n' | '\r' if self.level == Level::MacroValue => {
                    self.level = Level::Macro;
                    Some(Token::new(
                        TT::EndMacroValue("".to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                // Whitespace
                ws if ws.is_whitespace() => self.whitespace(start,line,column),
                // MPR Section headers
                '[' => {
                    self.next_char();
                    let n = self.peek_char().cloned()
                        .expect("Unexpected end of input stream after [");

                    match self.level {
                        Level::Top | Level::Head | Level::Variable |
                        Level::Coordinate | Level::Contour | Level::Macro =>
                            match n {
                                'H' => {
                                    self.level = Level::Head;
                                    self.next_char();
                                    Some(Token::new(
                                        TT::Head("[H".to_string()),
                                        start,
                                        self.position,
                                        line,
                                        column))
                                },
                                'K' => {
                                    self.level = Level::Coordinate;
                                    self.next_char();
                                    Some(Token::new(
                                        TT::Coordinate("[K".to_string()),
                                        start,
                                        self.position,
                                        line,
                                        column))
                                }
                                d if d.is_digit(10) => {
                                    self.level = Level::Variable;
                                    Some(Token::new(
                                        TT::Variable(format!("[{}",self.eat_digits())),
                                        start,
                                        self.position,
                                        line,
                                        column))
                                },
                                _ => panic!("{}",self.error(n)),
                            },
                        _ => {
                            let c = self.next_char().unwrap();
                            panic!("{}",self.error(c));
                        },
                    }
                },
                '\\' => {
                    // Macro or Coordinate Names match \.+\ otherwise it
                    // is a comment
                    self.next_char();
                    match self.level {
                        Level::CoordinateName => self.coordinate_name(start,line,column),
                        Level::MacroName => self.macro_name(start,line,column),
                        _ => self.comment(start,line,column),
                    }

                },
                // MPR Contour Header Number
                ']' => {
                    match self.level {
                        Level::Top | Level::Head | Level::Variable |
                        Level::Coordinate | Level::Contour | Level::Macro => {
                            self.level = Level::ContourNumber;
                            Some(Token::new(
                                TT::ContourNumber(self.eat_digits()),
                                start,
                                self.position,
                                line,
                                column))},
                        _ => panic!("Unexpected level for Contour ]"),
                    }
                },
                // If we are at the top Level and starting with $ this is a
                // contour element identifier
                '$' if self.level == Level::ContourNumber || self.level == Level::Contour =>
                    self.contour_element_number(start,line,column),
                'K' if self.level == Level::ContourElementNumber =>
                    self.contour_element(start,line,column),
                _ if self.level == Level::ContourElement => {
                    self.level = Level::Contour;
                    Some(Token::new(
                        TT::ContourComment(self.eat_until_newline()),
                        start,
                        self.position,
                        line,
                        column))
                },
                // This signifies the end of the MPR File. Embedded component
                // information may come after
                '!' => {
                    match self.level {
                        Level::Top | Level::Head | Level::Variable |
                        Level::Coordinate | Level::Contour | Level::Macro => {
                            Some(Token::new(
                                TT::EOF(self.next_char()
                                               .expect("Cannot take ! from stream")
                                               .to_string()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        _ => panic!("Unexpected level for !")
                    }
                }
                // If we matched a descriptor and encounter an equals sign we
                // need to set our level to match the input now
                '=' => {
                    match self.level {
                        Level::HeadKey => {
                            self.level = Level::HeadEquals;
                            Some(Token::new(
                                TT::HeadEquals(self.next_char()
                                                      .expect("Cannot take = from stream")
                                                      .to_string()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::VariableKey => {
                            self.level = Level::VariableEquals;
                            Some(Token::new(
                                TT::VariableEquals(self.next_char()
                                                          .expect("Cannot take = from stream")
                                                          .to_string()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::CoordinateKey => {
                            self.level = Level::CoordinateEquals;
                            Some(Token::new(
                                TT::CoordinateEquals(self.next_char()
                                                            .expect("Cannot take = from stream")
                                                            .to_string()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::ContourKey => {
                            self.level = Level::ContourEquals;
                            Some(Token::new(
                                TT::ContourEquals(self.next_char()
                                                         .expect("Cannot take = from stream")
                                                         .to_string()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::MacroKey => {
                            self.level = Level::MacroEquals;
                            Some(Token::new(
                                TT::MacroEquals(self.next_char()
                                                       .expect("Cannot take = from stream")
                                                       .to_string()),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        _ => panic!("Unexpected level for =")
                    }
                },
                c if c == '"' && self.level == Level::HeadEquals => {
                    self.level = Level::HeadValue;
                    Some(Token::new(
                        TT::HeadValue(self.next_char().unwrap().to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                c if c != '"' && self.level == Level::HeadEquals => {
                    self.level = Level::HeadValue;
                    Some(Token::new(
                        TT::HeadValue("".to_string()),
                            start,
                        self.position,
                        line,
                        column))
                },
                c if c == '"' && self.level == Level::VariableEquals => {
                    self.level = Level::VariableValue;
                    Some(Token::new(
                        TT::VariableValue(self.next_char().unwrap().to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                c if c != '"' && self.level == Level::VariableEquals => {
                    self.level = Level::VariableValue;
                    Some(Token::new(
                        TT::VariableValue("".to_string()),
                            start,
                        self.position,
                        line,
                        column))
                },
                c if c == '"' && self.level == Level::CoordinateEquals => {
                    self.level = Level::CoordinateValue;
                    Some(Token::new(
                        TT::CoordinateValue(self.next_char().unwrap().to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                c if c != '"' && self.level == Level::CoordinateEquals => {
                    self.level = Level::CoordinateValue;
                    Some(Token::new(
                        TT::CoordinateValue("".to_string()),
                            start,
                        self.position,
                        line,
                        column))
                },
                c if c == '"' && self.level == Level::ContourEquals => {
                    self.level = Level::ContourValue;
                    Some(Token::new(
                        TT::ContourValue(self.next_char().unwrap().to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                c if c != '"' && self.level == Level::ContourEquals => {
                    self.level = Level::ContourValue;
                    Some(Token::new(
                        TT::ContourValue("".to_string()),
                            start,
                        self.position,
                        line,
                        column))
                },
                c if c == '"' && self.level == Level::MacroEquals => {
                    self.level = Level::MacroValue;
                    Some(Token::new(
                        TT::MacroValue(self.next_char().unwrap().to_string()),
                        start,
                        self.position,
                        line,
                        column))
                },
                c if c != '"' && self.level == Level::MacroEquals => {
                    self.level = Level::MacroValue;
                    Some(Token::new(
                        TT::MacroValue("".to_string()),
                            start,
                        self.position,
                        line,
                        column))
                },
                // Match descriptor if we are at the Top Level
                // TODO: Descriptor valid identifiers aren't specified so I'm
                // guessing here
                'A'..='Z' | 'a'..='z'| '0'..='9' | '_' | '.' | '?' => {
                    let identifier = self.eat_identifier();

                    match identifier.as_ref() {
                        "VERSION" | "KM" | "WW" | "EA" | "EE" | "KAT" | "MNM" | "FN" |
                        "IN"
                            => self.next_literal = true,
                        _ => self.next_literal = false,
                    };

                    match self.level {
                        Level::Head => {
                            self.level = Level::HeadKey;
                            Some(Token::new(
                                TT::HeadKey(identifier),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::Variable => {
                            self.level = Level::VariableKey;
                            Some(Token::new(
                                TT::VariableKey(identifier),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::Coordinate => {
                            self.level = Level::CoordinateKey;
                            Some(Token::new(
                                TT::CoordinateKey(identifier),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::Contour => {
                            self.level = Level::ContourKey;
                            Some(Token::new(
                                TT::ContourKey(identifier),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        Level::Macro => {
                            self.level = Level::MacroKey;
                            Some(Token::new(
                                TT::MacroKey(identifier),
                                start,
                                self.position,
                                line,
                                column))
                        },
                        _ => {
                            panic!("Unmatched Level for Identifier: {}",identifier);
                        },
                    }
                },
                e => panic!("Unmatched Character: {}",self.error(e)),
            },
            None => None,
        };

        self.peeked = match token {
            Some(t) => Some(Box::new(t)),
            None => None,
        };

        &self.peeked
    }

    pub fn peek_owned(&mut self) -> Option<Token> {
        match self.peek() {
            Some(t) => Some(*t.clone()),
            None => None,
        }
    }
}

impl<'a> Iterator for Tokenizer<'a> {
    type Item = Box<Token>;

    fn next(&mut self) -> Option<Box<Token>> {
        if self.peeked.is_none() {
            self.peek();
        }
        let token = self.peeked.as_ref().cloned();
        self.peeked = None;
        token
    }
}
